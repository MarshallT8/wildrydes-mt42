window._config = {
    cognito: {
        userPoolId: 'eu-west-2_N5eWhqplY', // e.g. us-east-2_uXboG5pAb
        userPoolClientId: '4dcp3rjabk2r7gfj1vs5di42rr', // e.g. 25ddkmj4v6hfsfvruhpfi7n4hv
        region: 'eu-west-2' // e.g. us-east-2
    },
    api: {
        invokeUrl: 'https://472ujyf0eh.execute-api.eu-west-2.amazonaws.com/prod' // e.g. https://rc7nyt4tql.execute-api.us-west-2.amazonaws.com/prod',
    }
};
